package org.trumosteam.trumos.mixin;

import net.minecraft.network.play.server.SEntityTeleportPacket;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(SEntityTeleportPacket.class)
public interface SEntityTeleportPacketAccessor {
    @Accessor("x") double accessor$x();
    @Accessor("y") double accessor$y();
    @Accessor("z") double accessor$z();

    @Accessor("yRot") byte accessor$yRot();
    @Accessor("xRot") byte accessor$xRot();

    @Accessor("x") void accessor$setX(double x);
    @Accessor("y") void accessor$setY(double y);
    @Accessor("z") void accessor$setZ(double z);

    @Accessor("yRot") void accessor$setYRot(byte yRot);
    @Accessor("xRot") void accessor$setXRot(byte xRot);
}
