package org.trumosteam.trumos.api.model;

import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.api.registry.RegistryKey;
import org.spongepowered.api.registry.RegistryScope;
import org.spongepowered.api.registry.RegistryScopes;

@RegistryScopes(scopes = RegistryScope.GAME)
public class ModelInteractionTypes {
    public static final DefaultedRegistryReference<ModelInteractionType> NONE = ModelInteractionTypes.key(ResourceKey.of("models", "none"));
    public static final DefaultedRegistryReference<ModelInteractionType> SIT = ModelInteractionTypes.key(ResourceKey.of("models", "sit"));

    private ModelInteractionTypes() {}

    private static DefaultedRegistryReference<ModelInteractionType> key(final ResourceKey location) {
        return RegistryKey.of(Model.INTERACTION_TYPE_REGISTRY, location).asDefaultedReference(() -> Sponge.getGame().registries());
    }
}
