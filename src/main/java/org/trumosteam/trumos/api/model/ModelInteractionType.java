package org.trumosteam.trumos.api.model;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.registry.DefaultedRegistryValue;
import org.spongepowered.api.util.annotation.CatalogedBy;

@CatalogedBy(ModelInteractionTypes.class)
public interface ModelInteractionType extends DefaultedRegistryValue {
    default void onHit(final Player player, final Entity entity, final Model model) {}

    default void onInteract(Player player, final Entity entity, final Model model) {}
}
