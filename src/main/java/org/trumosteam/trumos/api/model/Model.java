package org.trumosteam.trumos.api.model;

import io.leangen.geantyref.TypeToken;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.Key;
import org.spongepowered.api.data.persistence.DataSerializable;
import org.spongepowered.api.data.value.Value;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.registry.DefaultedRegistryType;
import org.spongepowered.api.registry.RegistryRoots;
import org.spongepowered.api.registry.RegistryType;
import org.spongepowered.api.world.server.ServerLocation;
import org.spongepowered.math.vector.Vector3d;
import org.trumosteam.trumos.model.ModelBuilderImpl;

import java.util.Optional;

public interface Model extends DataSerializable {
    DefaultedRegistryType<ModelInteractionType> INTERACTION_TYPE_REGISTRY = RegistryType.of(RegistryRoots.SPONGE, ResourceKey.of("models", "interaction_type")).asDefaultedType(() -> Sponge.getGame().registries());

    Key<Value<Model>> DATA = Key.of(ResourceKey.of("models", "data"), new TypeToken<Value<Model>>() {});

    /**
     * Creates a new {@link Model.Builder} to build a {@link Model}.
     *
     * @return The new builder
     */
    static Builder builder() {
        return new ModelBuilderImpl();
    }

    /**
     * Gets the {@link ItemType} of this model.
     *
     * @return The {@link ItemType} of this model
     */
    ItemType getItemType();

    /**
     * Gets the path of this model.
     *
     * @return The path of this model
     */
    String getModelPath();

    /**
     * Gets the model settings if available
     *
     * @return The model settings, if available
     */
    Optional<ModelSettings> getSettings();

    /**
     * Sets the model settings of this model
     *
     * @param settings of this model
     */
    void setSettings(final ModelSettings settings);

    /**
     * Creates the {@link ItemStack} which looks like this model
     *
     * @return The {@link ItemStack} representation of this model
     */
    ItemStack toItemStack(final boolean withSettings);
    ItemStack toItemStack();

    /**
     * Creates the {@link EntityArchetype} based on model settings
     *
     * @return The {@link EntityArchetype} representation of this model
     */
    EntityArchetype toArchetype();

    Optional<Entity> spawnEntity(final ServerLocation location, final Vector3d rotation);

    interface Builder {

        Builder from(final Model model);

        /**
         * Sets the {@link ItemType} for this model.
         *
         * @param itemType The {@link ItemType} for the model
         * @return This builder
         */
        Builder itemType(final ItemType itemType);

        /**
         * Sets the path for this model file
         *
         * @param modelPath The path for the model file
         * @return This builder
         */
        Builder modelPath(final String modelPath);

        /**
         * Sets the settings for this model (Used when placed)
         *
         * @param settings The model settings
         * @return This builder
         */
        Builder modelSettings(final ModelSettings settings);

        /**
         * Builds an instance of a {@link Model}.
         *
         * @return A new instance of a {@link Model}
         * @throws IllegalStateException if the {@link Model} is not complete
         */
        Model build() throws IllegalStateException;
    }
}
