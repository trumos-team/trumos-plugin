package org.trumosteam.trumos.api.model;

import com.google.common.collect.ImmutableMap;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.resourcepack.ResourcePack;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public interface ModelService {
    /**
     * Register this model to the model system.
     *
     * @param itemType The {@link ItemType} being base of this model
     * @param modelPath The path to model
     */
    void registerModel(final ItemType itemType, final String modelPath);
    void registerModel(final Model model);

    /**
     * Unregisters this model from the model system.
     *
     * <p>A model can still be fully used after being unregistered. However,
     * it will not show it's visual properly in the game in any way until registered to a
     * {@link ModelService} again, through
     * {@link ModelService#registerModel(Model)}.</p>
     *
     * @return Whether this model was registered to a {@link ModelService}.
     */
    boolean unregisterModel(final ItemType itemType, final String modelPath);
    boolean unregisterModel(final Model model);

    /**
     * Registers this model settings to the model system.
     *
     * @param settings The {@link ModelSettings} to be registered
     */
    void registerSettings(final ModelSettings settings) throws SQLException, IOException;

    /**
     * Unregisters this model settings to the model system.
     *
     * @param settings The {@link ModelSettings} to be registered
     */
    boolean unregisterSettings(final ModelSettings settings);

    /**
     * Returns the registered {@link ModelSettings} settings if present
     *
     * @param name
     * @return {@link ModelSettings} settings, if present
     */
    Optional<ModelSettings> getSettings(final String name);

    /**
     * Returns immutable copy of map with all settings
     *
     * @param name
     * @return {@link ModelSettings} settings, if present
     */
    ImmutableMap<String, ModelSettings> getAllSettings();

    /**
     * Returns the custom model data value from registered {@link Model}
     *
     * @param model
     * @return {@link Integer} custom model data value, if present
     * @throws IllegalArgumentException if {@link Model} is not registered
     */
    Integer getCustomModelData(final Model model) throws IllegalArgumentException;

    /**
     * Returns Resource-Pack specified in settings if present
     *
     * @return {@link ResourcePack}, if present
     */
    Optional<ResourcePack> getResourcePack();

    /**
     * Builds Resource-Pack based on registered models
     *
     * @throws IOException
     */
    void buildResourcePack() throws IOException;

    /**
     * Uploads already prepared Resource-Pack
     *
     * @throws IOException
     */
    void uploadResourcePack() throws IOException;
}
