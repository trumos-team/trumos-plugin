package org.trumosteam.trumos.api.model;

import org.spongepowered.api.data.persistence.DataSerializable;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.math.vector.Vector3d;
import org.spongepowered.math.vector.Vector3i;
import org.trumosteam.trumos.model.ModelSettingsBuilderImpl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface ModelSettings extends DataSerializable {

    /**
     * Creates a new {@link ModelSettings.Builder} to build a {@link ModelSettings}.
     *
     * @return The new builder
     */
    static ModelSettings.Builder builder() {
        return new ModelSettingsBuilderImpl();
    }

    /**
     * Gets the unique identificator of this {@link ModelSettings} settings
     *
     * @return The unique name of this {@link ModelSettings}
     */
    String getName();

    /**
     * Gets the interaction behaviour object of this model when spawned as {@link Entity}
     *
     * @return The interaction behaviour of this {@link ModelSettings}
     */
    DefaultedRegistryReference<ModelInteractionType> getInteractionType();

    /**
     * Gets the offset of this model when spawned as {@link Entity}
     *
     * @return The {@link Vector3d} offset of spawned model
     */
    Vector3d getOffset();

    /**
     * Gets the collision points of this model when spawned as {@link Entity}
     *
     * @return The list of {@link Vector3i} points, being the collision of spawned model
     */
    List<Vector3i> getCollisionPoints();

    /**
     * Sets the interaction behaviour of this model when spawned as {@link Entity}
     *
     * @param interaction The interaction behaviour of this {@link ModelSettings}
     */
    void setInteractionType(final DefaultedRegistryReference<ModelInteractionType> interactionType);
    void setInteractionType(final ModelInteractionType interactionType);

    /**
     * Sets the offset of this model when spawned as {@link Entity}
     *
     * @param offset of this model
     */
    void setOffset(final Vector3d offset);

    /**
     * Sets the collision points of this model, which prevents {@link Entity}'ies from moving there
     *
     * @param points defining collision of this model
     */
    void setCollisionPoints(final List<Vector3i> points);
    void setCollisionPoints(final Vector3i... points);

    /**
     * Saves the current model settings, to make them persistent between restarts
     *
     * @throws IllegalArgumentException when not registered
     */
    void save() throws IOException, SQLException;

    interface Builder {

        ModelSettings.Builder from(final ModelSettings settings);

        /**
         * Sets the interaction behaviour for this model when being hit or interacted as {@link Entity}
         *
         * @param interactionType The interaction behaviour for this model
         * @return This builder
         */
        ModelSettings.Builder interactionType(final DefaultedRegistryReference<ModelInteractionType> interactionType);
        ModelSettings.Builder interactionType(final ModelInteractionType interactionType);

        /**
         * Sets the offset for this model when spawned as {@link Entity}
         *
         * @param offset The offset for the model entity
         * @return This builder
         */
        ModelSettings.Builder offset(final Vector3d offset);

        /**
         * Sets the block position points which will be preventing {@link Entity}'ies from moving in here
         *
         * @param points The points defining collision for this model
         * @return This builder
         */
        ModelSettings.Builder collisionPoints(final List<Vector3i> points);
        ModelSettings.Builder collisionPoints(final Vector3i... points);

        /**
         * Builds an instance of a {@link ModelSettings}.
         *
         * @param name The unique id representing this display settings
         * @return A new instance of a {@link ModelSettings}
         * @throws IllegalStateException if the {@link ModelSettings} is not complete
         */
        ModelSettings build(final String name) throws IllegalStateException;
    }
}
