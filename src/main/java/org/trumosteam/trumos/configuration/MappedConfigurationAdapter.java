package org.trumosteam.trumos.configuration;

import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.ConfigurationOptions;
import org.spongepowered.configurate.hocon.HoconConfigurationLoader;
import org.spongepowered.configurate.objectmapping.ObjectMapper;
import org.spongepowered.configurate.serialize.SerializationException;

import java.nio.file.Path;

public final class MappedConfigurationAdapter<T> {
    private final Class<T> configClass;
    private final Path configFile;
    private final HoconConfigurationLoader loader;
    private final ObjectMapper<T> mapper;

    private ConfigurationNode root;
    private T config;

    public MappedConfigurationAdapter(final Class<T> configClass, final ConfigurationOptions options, final Path configFile) {
        this.configClass = configClass;
        this.configFile = configFile;
        this.loader = HoconConfigurationLoader.builder()
                .defaultOptions(options)
                .path(configFile)
                .build();
        try {
            this.mapper = ObjectMapper.factory().get(configClass);
        } catch (final SerializationException e) {
            throw new RuntimeException(String.format("Failed to construct mapper for config class '%s'!", this.configClass));
        }
        this.root = this.loader.createNode(options);
        try {
            this.config = this.mapper.load(this.root);
        } catch (final SerializationException e) {
            throw new RuntimeException(e);
        }
    }

    public Class<T> getConfigClass() {
        return this.configClass;
    }

    public Path getConfigFile() {
        return this.configFile;
    }

    public T getConfig() {
        return this.config;
    }

    public void load() throws ConfigurateException {
        this.root = this.loader.load();
        this.config = this.mapper.load(this.root);
    }

    public void save() throws ConfigurateException {
        this.mapper.save(this.config, this.root);
        this.loader.save(this.root);
    }
}
