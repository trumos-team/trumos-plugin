package org.trumosteam.trumos;

import com.google.inject.Inject;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Server;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.Command;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.data.DataManager;
import org.spongepowered.api.data.DataRegistration;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.lifecycle.*;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.plugin.PluginContainer;
import org.spongepowered.plugin.jvm.Plugin;
import org.trumosteam.trumos.api.model.Model;
import org.trumosteam.trumos.api.model.ModelService;
import org.trumosteam.trumos.api.model.ModelSettings;
import org.trumosteam.trumos.model.ModelImpl;
import org.trumosteam.trumos.model.ModelManager;
import org.trumosteam.trumos.model.ModelServiceImpl;
import org.trumosteam.trumos.model.ModelSettingsImpl;
import org.trumosteam.trumos.model.editor.ModelEditor;
import org.trumosteam.trumos.model.interaction.ModelInteractionNone;
import org.trumosteam.trumos.model.interaction.ModelInteractionSit;

import java.nio.file.Path;
import java.sql.SQLException;

@Plugin("trumos")
public class Trumos {
    public static Trumos instance;

    private final PluginContainer plugin;
    private final Path configFile;

    private ModelServiceImpl modelService;
    private ModelEditor modelEditor;

    @Inject
    public Trumos(final PluginContainer plugin, @ConfigDir(sharedRoot = false) final Path configFile) {
        Trumos.instance = this;

        this.plugin = plugin;
        this.configFile = configFile;

        //Create config directory
        this.configFile.toFile().mkdirs();

        //Preload JDBC
        try { Class.forName("org.sqlite.JDBC"); } catch (ClassNotFoundException ignored) {}

        try {
            this.modelService = new ModelServiceImpl();
        } catch (SQLException e) {
            this.plugin.getLogger().error("Error occurred while initializing Model Service: ", e);
        }
    }

    public PluginContainer getPlugin() {
        return this.plugin;
    }

    public Path getConfigFile() {
        return this.configFile;
    }

    public ModelService getModelService() {
        return this.modelService;
    }

    public ModelEditor getModelEditor() {
        return this.modelEditor;
    }

    @Listener
    public void onServerStarted(final StartedEngineEvent<Server> event) {
        this.modelService.load();

        /*try {
            modelEditor = new ModelEditor("model_editor_world");
            Sponge.getEventManager().registerListeners(plugin, modelEditor);

        } catch (IOException e) {
            this.getPlugin().getLogger().error("Error occurred while trying to create world for model editor: ", e);
        }*/

        Sponge.getEventManager().registerListeners(this.plugin, new ModelManager());
    }

    @Listener
    public void onStoppingServer(final StoppingEngineEvent<Server> event) {

    }

    @Listener
    public void onRegisterCommand(final RegisterCommandEvent<Command.Parameterized> event) {
        event.register(this.plugin, Commands.rootCommand(), "models", "mdl");
    }

    @Listener
    public void onRegisterData(final RegisterDataEvent event) {
        this.plugin.getLogger().info("Trumos:onRegisterData");
        final DataManager dataManager = Sponge.getDataManager();

        dataManager.registerBuilder(Model.class, new ModelImpl.Builder());
        dataManager.registerBuilder(ModelSettings.class, new ModelSettingsImpl.Builder());

        event.register(DataRegistration.of(Model.DATA, ItemStack.class, ArmorStand.class));
    }

    @Listener
    public void onRegisterGameScopedRegistry(final RegisterRegistryEvent.GameScoped event) {
        event.register(ResourceKey.of("models", "interaction_type"), true);
    }

    @Listener
    public void onRegisterGameScoped(final RegisterRegistryValueEvent.GameScoped event) {
        event.registry(Model.INTERACTION_TYPE_REGISTRY)
                .register(ResourceKey.of("models", "none"), new ModelInteractionNone())
                .register(ResourceKey.of("models", "sit"), new ModelInteractionSit());
    }
}
