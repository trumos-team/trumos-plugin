package org.trumosteam.trumos.model;

import org.spongepowered.api.item.ItemType;
import org.trumosteam.trumos.api.model.Model;
import org.trumosteam.trumos.api.model.ModelSettings;

public class ModelBuilderImpl implements Model.Builder {
    private ItemType itemType;
    private String modelPath;
    private String modelSettings;

    @Override
    public Model.Builder from(final Model model) {
        return this
                .itemType(model.getItemType())
                .modelPath(model.getModelPath());
    }

    @Override
    public Model.Builder itemType(final ItemType itemType) {
        this.itemType = itemType;
        return this;
    }

    @Override
    public Model.Builder modelPath(final String modelPath) {
        this.modelPath = modelPath;
        return this;
    }

    @Override
    public Model.Builder modelSettings(final ModelSettings settings) {
        this.modelSettings = settings.getName();
        return this;
    }

    @Override
    public Model build() throws IllegalStateException {
        if (this.itemType == null)
            throw new IllegalStateException("Missing ItemType value! Use itemType(ItemType)");

        if (this.modelPath == null)
            throw new IllegalStateException("Missing Model Path value! Use modelPath(path)");

        return new ModelImpl(this.itemType, this.modelPath, this.modelSettings);
    }
}

