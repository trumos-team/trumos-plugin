package org.trumosteam.trumos.model;

import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.math.vector.Vector3d;
import org.spongepowered.math.vector.Vector3i;
import org.trumosteam.trumos.api.model.Model;
import org.trumosteam.trumos.api.model.ModelInteractionType;
import org.trumosteam.trumos.api.model.ModelInteractionTypes;
import org.trumosteam.trumos.api.model.ModelSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ModelSettingsBuilderImpl implements ModelSettings.Builder {
    private Vector3d offset;
    private List<Vector3i> collision;
    private DefaultedRegistryReference<ModelInteractionType> interaction;

    public ModelSettingsBuilderImpl() {
        offset = Vector3d.ZERO;
    }

    @Override
    public ModelSettings.Builder from(ModelSettings display) {
        return this
                .offset(display.getOffset())
                .collisionPoints(display.getCollisionPoints())
                .interactionType(display.getInteractionType());
    }

    @Override
    public ModelSettings.Builder interactionType(DefaultedRegistryReference<ModelInteractionType> interactionType) {
        this.interaction = interactionType;
        return this;
    }

    @Override
    public ModelSettings.Builder interactionType(ModelInteractionType interactionType) {
        this.interaction = Model.INTERACTION_TYPE_REGISTRY.defaultReferenced(Model.INTERACTION_TYPE_REGISTRY.get().valueKey(interactionType));;
        return this;
    }

    @Override
    public ModelSettings.Builder offset(Vector3d offset) {
        this.offset = offset;
        return this;
    }

    @Override
    public ModelSettings.Builder collisionPoints(List<Vector3i> points) {
        this.collision = points;
        return this;
    }
    @Override
    public ModelSettings.Builder collisionPoints(Vector3i... points) {
        return this.collisionPoints(Arrays.asList(points));
    }

    @Override
    public ModelSettings build(String name) throws IllegalStateException {
        if(this.offset == null)
            this.offset = Vector3d.ZERO;

        if(this.collision == null)
            this.collision = new ArrayList<>();

        if(this.interaction == null)
            this.interaction = ModelInteractionTypes.NONE;

        return new ModelSettingsImpl(name, interaction, offset, collision);
    }
}
