package org.trumosteam.trumos.model;

import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.data.persistence.*;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.math.vector.Vector3d;
import org.spongepowered.math.vector.Vector3i;
import org.trumosteam.trumos.Trumos;
import org.trumosteam.trumos.api.model.Model;
import org.trumosteam.trumos.api.model.ModelInteractionType;
import org.trumosteam.trumos.api.model.ModelInteractionTypes;
import org.trumosteam.trumos.api.model.ModelSettings;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ModelSettingsImpl implements ModelSettings {
    private static final DataQuery SETTINGS_NAME = DataQuery.of("name");
    private static final DataQuery SETTINGS_COLLISION = DataQuery.of("collision");
    private static final DataQuery SETTINGS_OFFSET = DataQuery.of("offset");
    private static final DataQuery SETTINGS_INTERACTION_TYPE = DataQuery.of("interaction_type");

    private final String name;
    private final List<Vector3i> collision;
    private DefaultedRegistryReference<ModelInteractionType> interaction;
    private Vector3d offset;

    ModelSettingsImpl(final String name, final DefaultedRegistryReference<ModelInteractionType> interaction, final Vector3d offset, final List<Vector3i> collision) {
        this.name = name;
        this.collision = collision;
        this.interaction = interaction;
        this.offset = offset;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public DefaultedRegistryReference<ModelInteractionType> getInteractionType() { return interaction; }

    @Override
    public Vector3d getOffset() {
        return offset;
    }

    @Override
    public List<Vector3i> getCollisionPoints() {
        return collision;
    }

    @Override
    public void setInteractionType(DefaultedRegistryReference<ModelInteractionType> interaction) {
        this.interaction = interaction;
    }

    @Override
    public void setInteractionType(ModelInteractionType interaction) throws IllegalStateException {
        this.interaction = Model.INTERACTION_TYPE_REGISTRY.defaultReferenced(Model.INTERACTION_TYPE_REGISTRY.get().valueKey(interaction));
    }

    @Override
    public void setOffset(Vector3d offset) {
        this.offset = offset;
    }

    @Override
    public void setCollisionPoints(List<Vector3i> points) {
        collision.clear();
        collision.addAll(points);
    }

    @Override
    public void setCollisionPoints(Vector3i... points) {
        setCollisionPoints(Arrays.asList(points));
    }

    @Override
    public void save() throws IOException, SQLException {
        Trumos.instance.getModelService().registerSettings(this);
    }

    @Override
    public int getContentVersion() {
        return Builder.VERSION;
    }

    @Override
    public DataContainer toContainer() {
        return DataContainer.createNew()
                .set(SETTINGS_NAME, name)
                .set(SETTINGS_COLLISION, collision)
                .set(SETTINGS_OFFSET, offset)
                .set(SETTINGS_INTERACTION_TYPE, interaction);
    }

    public static class Builder extends AbstractDataBuilder<ModelSettings> {
        public final static int VERSION = 1;

        public Builder() {
            super(ModelSettings.class, VERSION);
        }

        @Override
        protected Optional<ModelSettings> buildContent(DataView container) throws InvalidDataException {
            if(!container.contains(SETTINGS_NAME))
                return Optional.empty();

            String name = container.getString(SETTINGS_NAME).get();
            Vector3d offset = container.getObject(SETTINGS_OFFSET, Vector3d.class).orElse(Vector3d.ZERO);
            List<Vector3i> collision = container.getObjectList(SETTINGS_COLLISION, Vector3i.class).orElseGet(ArrayList::new);

            Optional<ResourceKey> interactionKey = container.getResourceKey(SETTINGS_INTERACTION_TYPE);
            DefaultedRegistryReference<ModelInteractionType> interaction = interactionKey.map(Model.INTERACTION_TYPE_REGISTRY::defaultReferenced).orElse(ModelInteractionTypes.NONE);

            return Optional.of(new ModelSettingsImpl(name, interaction, offset, collision));
        }
    }
}
