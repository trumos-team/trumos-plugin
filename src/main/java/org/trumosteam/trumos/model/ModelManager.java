package org.trumosteam.trumos.model;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.Keys;
import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.event.EventContextKeys;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.ChangeEntityEquipmentEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.entity.RotateEntityEvent;
import org.spongepowered.api.event.filter.Getter;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.event.network.ServerSideConnectionEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.Hotbar;
import org.spongepowered.api.scoreboard.Scoreboard;
import org.spongepowered.api.scoreboard.Team;
import org.spongepowered.api.util.blockray.RayTrace;
import org.spongepowered.api.util.blockray.RayTraceResult;
import org.spongepowered.api.world.LocatableBlock;
import org.spongepowered.api.world.server.ServerLocation;
import org.spongepowered.api.world.server.ServerWorld;
import org.spongepowered.math.vector.Vector3d;
import org.trumosteam.trumos.Trumos;
import org.trumosteam.trumos.api.model.Model;
import org.trumosteam.trumos.model.editor.ModelHologram;
import org.trumosteam.trumos.util.Packets;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class ModelManager {
    private final String MODEL_HOLOGRAM_NAME = "model_hologram";
    private final Vector3d BLOCK_POSITION_OFFSET = new Vector3d(0.5d, 0d, 0.5d);

    private ArmorStand MODEL_PREVIEW;
    private Team MODEL_TEAM;

    private Map<UUID, ModelHologram> viewingPlayers;

    public ModelManager() {
        final ServerWorld world = Sponge.getServer().getWorldManager().defaultWorld();

        MODEL_PREVIEW = world.createEntity(EntityTypes.ARMOR_STAND, Vector3d.ZERO);
        MODEL_PREVIEW.offer(Keys.DISPLAY_NAME, Component.text(MODEL_HOLOGRAM_NAME));
        MODEL_PREVIEW.offer(Keys.IS_SMALL, true);
        MODEL_PREVIEW.offer(Keys.HAS_MARKER, true);
        MODEL_PREVIEW.offer(Keys.IS_GRAVITY_AFFECTED, false);
        //MODEL_PREVIEW.offer(Keys.IS_INVISIBLE, true);
        MODEL_PREVIEW.offer(Keys.IS_GLOWING, true);

        final Scoreboard scoreboard = Sponge.getServer().getServerScoreboard()
                .orElseThrow(() -> new RuntimeException("ModelManager registered too early!"));

        MODEL_TEAM = scoreboard.getTeam(MODEL_HOLOGRAM_NAME)
                .orElseGet(() -> {
                    Team team = Team.builder()
                            .name(MODEL_HOLOGRAM_NAME)
                            .build();

                    scoreboard.registerTeam(team);
                    return team;
                });

        MODEL_TEAM.getMembers().forEach(MODEL_TEAM::removeMember);
        MODEL_TEAM.addMember(MODEL_PREVIEW.getTeamRepresentation());
        MODEL_TEAM.setColor(NamedTextColor.GREEN);

        viewingPlayers = new HashMap<>();
    }

    public void showHologram(final ServerPlayer player, final Model model) {
        Trumos.instance.getPlugin().getLogger().info("showHologram");
        viewingPlayers.put(player.getUniqueId(), new ModelHologram(model));

        Packets.spawnEntity(player, MODEL_PREVIEW, (ServerLocation) player.getLocation(), player.getRotation());
        Packets.setEntityHelmet(player, MODEL_PREVIEW, model.toItemStack());

        updateHologram(player);
    }

    public void hideHologram(final ServerPlayer player) {
        Trumos.instance.getPlugin().getLogger().info("hideHologram");
        viewingPlayers.remove(player.getUniqueId());

        Packets.unSpawnEntity(player, MODEL_PREVIEW);
    }

    private void updateHologram(final ServerPlayer player) {
        Trumos.instance.getPlugin().getLogger().info("updateHologram");

        if(!viewingPlayers.containsKey(player.getUniqueId()))
            return;

        final Optional<RayTraceResult<LocatableBlock>> rayTraceResult = RayTrace.block()
                .sourceEyePosition(player)
                .direction(player)
                .select(RayTrace.nonAir())
                .limit(5)
                .execute();

        if(rayTraceResult.isPresent()) {
            final ModelHologram hologram = viewingPlayers.get(player.getUniqueId());
            final Vector3d offset = hologram.model.getSettings().get().getOffset();

            hologram.position = rayTraceResult.get().getHitPosition();
            hologram.rotation = player.headRotation().get();

            if(!offset.equals(Vector3d.ZERO)) {
                final double angle = Math.toRadians(hologram.rotation.getY());
                final double cos = Math.cos(angle), sin = Math.sin(angle);

                hologram.position = hologram.position.add(new Vector3d(
                        (cos * offset.getX()) - (sin * offset.getZ()),
                        offset.getY(),
                        (cos * offset.getZ()) + (sin * offset.getX())));
            }

            if(player.get(Keys.IS_SNEAKING).orElse(false)) {
                hologram.rotation = new Vector3d(hologram.rotation.getX(), 90*Math.round(hologram.rotation.getY()/90d), hologram.rotation.getZ());
                hologram.position = hologram.position.toInt().toDouble().add(BLOCK_POSITION_OFFSET);
            }

            Packets.teleportEntity(player, MODEL_PREVIEW, (ServerLocation) player.getLocation().withPosition(hologram.position), hologram.rotation);
        }
    }

    private void placeModel(final ServerPlayer player) {
        if(!viewingPlayers.containsKey(player.getUniqueId()))
            return;

        final ModelHologram hologram = viewingPlayers.get(player.getUniqueId());
        final ItemStack itemStack = player.getItemInHand(HandTypes.MAIN_HAND);

        hologram.model.spawnEntity(player.getServerLocation().withPosition(hologram.position), hologram.rotation)
                .ifPresent(entity -> {
                    entity.offer(Model.DATA, hologram.model);
                });

        if(!itemStack.isEmpty() && !player.get(Keys.GAME_MODE).orElse(GameModes.NOT_SET.get()).equals(GameModes.CREATIVE.get())) {
            final Hotbar hotbar = player.getInventory().getHotbar();

            hotbar.getSlot(hotbar.getSelectedSlotIndex())
                    .ifPresent(slot -> slot.poll(1));

            if(itemStack.getQuantity() <= 1)
                hideHologram(player);
        }
    }

    @Listener
    public void onPlayerDisconnect(final ServerSideConnectionEvent.Disconnect event) {
        if(viewingPlayers.containsKey(event.getPlayer().getUniqueId()))
            hideHologram(event.getPlayer());
    }

    @Listener
    public void onPlayerMove(final MoveEntityEvent event, final @Getter("getEntity") ServerPlayer player) {
        if(!viewingPlayers.containsKey(player.getUniqueId()))
            return;

        updateHologram(player);
    }

    @Listener
    public void onPlayerRotate(final RotateEntityEvent event, final @Getter("getEntity") ServerPlayer player) {
        if(!viewingPlayers.containsKey(player.getUniqueId()))
            return;

        updateHologram(player);
    }

    @Listener
    public void onEquipmentChange(final ChangeEntityEquipmentEvent event, final @Getter("getEntity") ServerPlayer player) {
        final UUID uuid = player.getUniqueId();
        final Optional<Model> model = player.getItemInHand(HandTypes.MAIN_HAND).get(Model.DATA);

        if(viewingPlayers.containsKey(uuid) && viewingPlayers.get(uuid).model != model.orElse(null))
            hideHologram(player);

        if(model.isPresent() && !viewingPlayers.containsKey(uuid))
            showHologram(player, model.get());
    }

    @Listener
    public void onPlayerItemInteract(final InteractItemEvent.Secondary event, final @Root ServerPlayer player) {
        final HandType usedHand = event.getContext().get(EventContextKeys.USED_HAND).orElse(HandTypes.OFF_HAND.get());

        if(usedHand.equals(HandTypes.MAIN_HAND.get()) && viewingPlayers.containsKey(player.getUniqueId()))
            placeModel(player);
    }

    @Listener
    public void onEntityInteract(final InteractEntityEvent.Secondary event, final @Root ServerPlayer player, final @Getter("getEntity") ArmorStand entity) {
        final HandType usedHand = event.getContext().get(EventContextKeys.USED_HAND).orElse(HandTypes.OFF_HAND.get());

        if(!usedHand.equals(HandTypes.MAIN_HAND.get()))
            return;

        final UUID uuid = player.getUniqueId();

        if(!viewingPlayers.containsKey(uuid)) {
            final Optional<Model> model = entity.get(Model.DATA);

            if(model.isPresent()) {
                model.get().getSettings().ifPresent(settings ->
                        settings.getInteractionType().get().onInteract(player, entity, model.get()));
            }
        }
        else placeModel(player);
    }

    @Listener
    public void onEntityHit(final InteractEntityEvent.Primary event, final @Root ServerPlayer player, final @Getter("getEntity") ArmorStand entity) {
        final HandType usedHand = event.getContext().get(EventContextKeys.USED_HAND).orElse(HandTypes.OFF_HAND.get());

        if(usedHand.equals(HandTypes.MAIN_HAND.get()) && !viewingPlayers.containsKey(player.getUniqueId())) {
            final Optional<Model> model = entity.get(Model.DATA);

            if (model.isPresent()) {
                if(player.get(Keys.IS_SNEAKING).orElse(false)) {
                    player.getInventory().offer(model.get().toItemStack());
                    entity.remove();
                }
                else model.get().getSettings().ifPresent(settings ->
                        settings.getInteractionType().get().onHit(player, entity, model.get()));
            }
        }
    }
}
