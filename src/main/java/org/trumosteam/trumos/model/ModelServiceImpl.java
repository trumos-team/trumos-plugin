package org.trumosteam.trumos.model;

import com.google.common.collect.ImmutableMap;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.persistence.DataContainer;
import org.spongepowered.api.data.persistence.DataFormats;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.registry.RegistryTypes;
import org.spongepowered.api.resourcepack.ResourcePack;
import org.spongepowered.api.sql.SqlManager;
import org.trumosteam.trumos.Trumos;
import org.trumosteam.trumos.api.model.Model;
import org.trumosteam.trumos.api.model.ModelService;
import org.trumosteam.trumos.api.model.ModelSettings;
import org.trumosteam.trumos.model.json.JsonModel;
import org.trumosteam.trumos.util.ZipFile;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class ModelServiceImpl implements ModelService {
    public final Map<ItemType, Map<String, Integer>> modelRegistry = new HashMap<>();
    public final Map<String, ModelSettings> settingsRegistry = new HashMap<>();

    private final DataSource dataSource;
    private ResourcePack resourcePack;

    public ModelServiceImpl() throws SQLException {

        SqlManager sqlService = Sponge.getSqlManager();
        dataSource = sqlService.getDataSource("jdbc:sqlite:" + Trumos.instance.getConfigFile().toString() + "/registry.db");

        settingsRegistry.put("default", ModelSettings.builder().build("default"));
    }

    public void load() {
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();

            Trumos.instance.getPlugin().getLogger().info("Loaded " + loadModels(statement) + " models and " + loadSettings(statement) + " settings!");
            connection.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void registerModel(ItemType itemType, String modelPath) throws IllegalArgumentException {
        Map<String, Integer> models = modelRegistry.computeIfAbsent(itemType, k -> new HashMap<>());

        if(models.containsKey(modelPath))
            throw new IllegalArgumentException(modelPath + " is already registered for " + itemType.key(RegistryTypes.ITEM_TYPE));

        for(int i = 0; i < Integer.MAX_VALUE; ++i) {
            if(!models.containsValue(i)) {
                models.put(modelPath, i);
                modelRegistry.put(itemType, models);

                try {
                    Connection connection = dataSource.getConnection();
                    PreparedStatement st = connection.prepareStatement("INSERT INTO models VALUES (NULL, ?, ?, ?)");

                    st.setString(1, itemType.key(RegistryTypes.ITEM_TYPE).asString());
                    st.setInt(2, i);
                    st.setString(3, modelPath);
                    st.execute();

                    connection.close();

                }
                catch (SQLException ignored) {}
                return;
            }
        }
    }

    @Override
    public void registerModel(Model model) {
        this.registerModel(model.getItemType(), model.getModelPath());
    }

    @Override
    public boolean unregisterModel(ItemType itemType, String modelPath) {
        Map<String, Integer> models = modelRegistry.get(itemType);

        if(models != null)
        {
            if(models.remove(modelPath) != null) {
                try {
                    Connection connection = dataSource.getConnection();
                    PreparedStatement st = connection.prepareStatement("DELETE FROM models WHERE item_type = ? AND model_path = ?");
                    st.setString(1, itemType.key(RegistryTypes.ITEM_TYPE).asString());
                    st.setString(2, modelPath);
                    st.execute();

                    connection.close();
                }
                catch (SQLException ignored) {}

                return true;
            }
        }

        return false;
    }

    @Override
    public boolean unregisterModel(Model model) {
        return unregisterModel(model.getItemType(), model.getModelPath());
    }

    @Override
    public void registerSettings(final ModelSettings settings) throws SQLException, IOException {
        final Connection connection = dataSource.getConnection();

        PreparedStatement statement = null;

        if(settingsRegistry.containsKey(settings.getName()))
            statement = connection.prepareStatement("UPDATE settings SET json = ? WHERE name = ?");
        else
            statement = connection.prepareStatement("INSERT INTO settings(json,name) VALUES (?, ?)");

        statement.setString(1, DataFormats.JSON.get().write(settings.toContainer()));
        statement.setString(2, settings.getName());
        statement.execute();

        connection.close();

        settingsRegistry.put(settings.getName(), settings);
    }

    @Override
    public boolean unregisterSettings(ModelSettings settings) {
        ModelSettings modelSettings = settingsRegistry.remove(settings.getName());

        if(modelSettings != null) {
            try {
                Connection connection = dataSource.getConnection();
                PreparedStatement st = connection.prepareStatement("DELETE FROM settings WHERE name = ?");
                st.setString(1, settings.getName());
                st.execute();

                connection.close();
            } catch (SQLException ignored) {}

            return true;
        }

        return false;
    }

    @Override
    public Optional<ModelSettings> getSettings(String name) {
        return Optional.ofNullable(this.settingsRegistry.get(name));
    }

    @Override
    public ImmutableMap<String, ModelSettings> getAllSettings() {
        return ImmutableMap.<String, ModelSettings>builder().putAll(this.settingsRegistry).build();
    }

    @Override
    public Optional<ResourcePack> getResourcePack() {
        return Optional.ofNullable(resourcePack);
    }

    @Override
    public void buildResourcePack() throws IOException {
        String directory = Trumos.instance.getConfigFile().toString();

        ZipFile resourcePack = new ZipFile(directory + File.separator + "resource-pack.zip");
        File sources = new File(directory + File.separator + "resources");
        URI resource = sources.toURI();

        Map<String, ItemType> registeredItems = getRegisteredItemFiles();
        Deque<File> fileQueue = new LinkedList<>();
        fileQueue.push(sources);

        try {
            while (!fileQueue.isEmpty()) {
                sources = fileQueue.pop();

                File[] files = sources.listFiles();

                if(files == null)
                    throw new IOException("Assets folder is empty!");

                for(File child : files) {
                    String name = resource.relativize(child.toURI()).getPath();

                    if(child.isDirectory()) {
                        resourcePack.addDirectory(name);
                        fileQueue.push(child);
                    }
                    else {
                        if(registeredItems.containsKey(name)) {
                            registeredItems.remove(name);

                            JsonModel model = JsonModel.from(child);

                            modelRegistry.get(registeredItems.get(name))
                                    .forEach(model::addModelData);

                            resourcePack.addFile(name, model.toString());
                        }
                        else
                            resourcePack.addFile(name, child);
                    }
                }
            }

            if(!registeredItems.isEmpty()) {
                for(Map.Entry<String, ItemType> entry : registeredItems.entrySet()) {
                    JsonModel model = JsonModel.from(entry.getValue());

                    modelRegistry.get(entry.getValue())
                            .forEach(model::addModelData);

                    resourcePack.addFile(entry.getKey(), model.toString());
                }
            }
        }
        finally {
            resourcePack.close();
        }
    }

    @Override
    public void uploadResourcePack() throws IOException {
        throw new UnsupportedOperationException("Not implemented!");
        /*String directory = Models.getInstance().getConfigDir().toString();
        Config config = Models.getInstance().getConfig();

        DbxClientV2 dropboxClient = new DbxClientV2(DbxRequestConfig.newBuilder("Sponge:Models/0.2").build(), config.getAccessToken());
        InputStream fs = new FileInputStream(directory + File.separator + "resource-pack.zip");

        try {
            dropboxClient.files()
                .uploadBuilder(config.getFilePath())
                .uploadAndFinish(fs);

            SharedLinkMetadata metadata = dropboxClient.sharing()
                    .createSharedLinkWithSettings(config.getFilePath());

            String dropboxLink = metadata.getUrl();
            dropboxLink = "https://dl.dropboxusercontent.com" + dropboxLink.substring(dropboxLink.indexOf("/s/"), dropboxLink.indexOf("?dl=0"));

            config.setResourcePackUrl(dropboxLink);
            resourcePack = ResourcePack.fromUri(URI.create(dropboxLink));

            for(ServerPlayer player : Sponge.getServer().getOnlinePlayers())
                player.sendResourcePack(resourcePack);

        } catch (DbxException e) {
            throw new IOException("Dropbox problem: " + e);
        }

        fs.close();*/
    }

    private long loadModels(Statement statement) throws SQLException {
        statement.execute("CREATE TABLE IF NOT EXISTS models (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, item_type VARCHAR(50) NOT NULL, model_id INT(11) NOT NULL, model_path VARCHAR(255) NOT NULL)");

        ResultSet result = statement.executeQuery("SELECT item_type, model_id, model_path FROM models");
        long counter = 0;

        while(result.next()) {
            Optional<ItemType> itemType = RegistryTypes.ITEM_TYPE.get().findValue(ResourceKey.resolve(result.getString("item_type")));

            if(!itemType.isPresent())
                continue;

            Map<String, Integer> entries = modelRegistry.computeIfAbsent(itemType.get(), k -> new HashMap<>());
            entries.put(result.getString("model_path"), result.getInt("model_id"));

            modelRegistry.put(itemType.get(), entries);
            counter++;
        }

        return counter;
    }

    private long loadSettings(Statement statement) throws SQLException {
        statement.execute("CREATE TABLE IF NOT EXISTS settings (name VARCHAR(50) UNIQUE NOT NULL PRIMARY KEY, json TEXT NOT NULL)");

        ResultSet result = statement.executeQuery("SELECT json FROM settings");
        long counter = 0;

        while(result.next()) {
            try {
                DataContainer container = DataFormats.JSON.get().read(result.getString("json"));

                Sponge.getDataManager().deserialize(ModelSettings.class, container)
                        .ifPresent(settings -> settingsRegistry.put(settings.getName(), settings));

                counter++;
            }
            catch (IOException ignored) {}
        }

        return counter;
    }

    public Map<String, ItemType> getRegisteredItemFiles() {
        return modelRegistry.entrySet().stream()
                .collect(Collectors.toMap(
                        e -> "assets/minecraft/item/" + e.getKey().key(RegistryTypes.ITEM_TYPE).getValue().substring(10) + ".json",
                        Map.Entry::getKey
                ));
    }

    @Override
    public Integer getCustomModelData(final Model model) throws IllegalArgumentException {
        return Optional.ofNullable(modelRegistry.get(model.getItemType()))
                .map(models -> models.get(model.getModelPath()))
                .orElseThrow(() -> new IllegalArgumentException("Model " + model + " is not registered!"));
    }
}