package org.trumosteam.trumos.model.json;

import com.google.common.io.Files;
import com.google.gson.*;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.registry.RegistryTypes;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonModel {
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static final JsonParser parser = new JsonParser();

    public String parent;
    public Boolean ambientocclusion;
    public String gui_light;

    public Map<String, String> textures;
    public Map<String, Element> elements;
    public Map<String, Display> display;
    public List<Override> overrides;

    public static JsonModel from(final String json) {
        JsonObject object = parser.parse(json).getAsJsonObject();
        return gson.fromJson(object, JsonModel.class);
    }

    public static JsonModel from(final File file) throws IOException {
        Reader reader = Files.newReader(file, Charset.defaultCharset());
        return gson.fromJson(reader, JsonModel.class);
    }

    public static JsonModel from(final ItemType itemType) {
        final ResourceKey resourceKey = Sponge.getGame().registries().registry(RegistryTypes.ITEM_TYPE).valueKey(itemType);
        final String typename = resourceKey.getValue().substring(10);

        JsonModel model = new JsonModel();
        model.parent = "item/generated";
        //model.addTexture("particle", typename);
        model.addTexture("layer0", "item/" + typename);

        return model;
    }

    public void addTexture(final String texturename, final String filename) {
        if(textures == null)
            textures = new HashMap<>();

        textures.put(texturename, filename);
    }

    public void addModelData(final String modelpath, final Integer modelid) {
        if(overrides != null) {
            JsonPrimitive defaultModelId = new JsonPrimitive(-1);

            for (Override override : overrides) {
                JsonPrimitive id = override.predicate.getOrDefault("custom_model_data", defaultModelId);

                if (id.getAsInt() == modelid)
                    throw new IllegalArgumentException("Model Id already in use! Id:" + modelid);
            }
        }
        else
            overrides = new ArrayList<>();

        Override override = new Override();
        override.model = modelpath;
        override.predicate = new HashMap<>();
        override.predicate.put("custom_model_data", new JsonPrimitive(modelid));

        overrides.add(override);
    }

    public static class Display {
        public JsonArray rotation;
        public JsonArray translation;
        public JsonArray scale;
    }

    public static class Element {
        public JsonArray from;
        public JsonArray to;
        public JsonObject faces;
    }

    public static class Override {
        public Map<String, JsonPrimitive> predicate;
        public String model;
    }

    @java.lang.Override
    public String toString() {
        return gson.toJson(this);
    }
}

