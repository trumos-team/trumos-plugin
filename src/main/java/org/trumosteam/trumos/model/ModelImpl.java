package org.trumosteam.trumos.model;

import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.Keys;
import org.spongepowered.api.data.persistence.*;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.equipment.EquipmentType;
import org.spongepowered.api.item.inventory.equipment.EquipmentTypes;
import org.spongepowered.api.registry.RegistryTypes;
import org.spongepowered.api.world.server.ServerLocation;
import org.spongepowered.math.vector.Vector3d;
import org.trumosteam.trumos.Trumos;
import org.trumosteam.trumos.api.model.Model;
import org.trumosteam.trumos.api.model.ModelSettings;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;

public class ModelImpl implements Model {
    private static final DataQuery ITEM_TYPE = DataQuery.of("item_type");
    private static final DataQuery MODEL_PATH = DataQuery.of("model_path");
    private static final DataQuery MODEL_SETTINGS = DataQuery.of("model_settings");

    private static final HashMap<EquipmentType, Boolean> LOCK_ALL_EQUIPMENT = new HashMap<EquipmentType, Boolean>() {{
        put(EquipmentTypes.HEAD.get(), false);
        put(EquipmentTypes.CHEST.get(), false);
        put(EquipmentTypes.LEGS.get(), false);
        put(EquipmentTypes.FEET.get(), false);
        put(EquipmentTypes.MAIN_HAND.get(), false);
        put(EquipmentTypes.OFF_HAND.get(), false);
    }};

    private final ItemType itemType;
    private final String modelPath;
    private String modelSettings;

    ModelImpl(final ItemType itemType, final String modelPath, final String modelSettings) {
        this.itemType = itemType;
        this.modelPath = modelPath;
        this.modelSettings = modelSettings;
    }

    @Override
    public ItemType getItemType() {
        return itemType;
    }

    @Override
    public String getModelPath() {
        return modelPath;
    }

    @Override
    public Optional<ModelSettings> getSettings() {
        return Trumos.instance.getModelService().getSettings(modelSettings);
    }

    @Override
    public void setSettings(final ModelSettings settings) {
        this.modelSettings = settings.getName();
    }

    @Override
    public ItemStack toItemStack(final boolean withSettings) {
        ItemStack itemStack = ItemStack.of(this.getItemType());

        if(withSettings)
            this.getSettings().ifPresent(info -> itemStack.offer(Model.DATA, this));

        try {
            itemStack.offer(Keys.CUSTOM_MODEL_DATA, Trumos.instance.getModelService().getCustomModelData(this));

        } catch (IllegalArgumentException e) {
            final ResourceKey resourceKey = Sponge.getGame().registries().registry(RegistryTypes.ITEM_TYPE).valueKey(itemType);
            Trumos.instance.getPlugin().getLogger().error("Used unregistered model {item_type:" + resourceKey.asString() + ", model_path:" + modelPath + "}. Re-register it in order to make it work properly!");
        }

        return itemStack;
    }

    @Override
    public ItemStack toItemStack() {
        return toItemStack(true);
    }

    @Override
    public EntityArchetype toArchetype() {
        DataContainer empty = DataContainer.createNew();

        DataContainer data = DataContainer.createNew()
                .set(DataQuery.of("ArmorItems"), Arrays.asList(empty, empty, empty, toItemStack().toContainer()));

        return EntityArchetype.builder()
                .type(EntityTypes.ARMOR_STAND)
                .entityData(data)
                .add(Keys.IS_GRAVITY_AFFECTED, false)
                .add(Keys.IS_SMALL, true)
                .add(Keys.IS_INVISIBLE, true)
                .build();
    }

    @Override
    public Optional<Entity> spawnEntity(final ServerLocation location, final Vector3d rotation) {
        ArmorStand entity = location.createEntity(EntityTypes.ARMOR_STAND.get());

        entity.setHead(toItemStack(false));
        entity.offer(Keys.IS_GRAVITY_AFFECTED, false);
        entity.offer(Keys.IS_SMALL, true);
        entity.offer(Keys.IS_INVISIBLE, true);
        entity.offer(Keys.IS_PLACING_DISABLED, LOCK_ALL_EQUIPMENT);
        entity.offer(Keys.IS_TAKING_DISABLED, LOCK_ALL_EQUIPMENT);
        entity.offer(Model.DATA, this);
        entity.setRotation(rotation);

        if(!location.getWorld().spawnEntity(entity))
            entity.remove();

        return entity.isRemoved() ? Optional.empty() : Optional.of(entity);
    }

    @Override
    public int getContentVersion() {
        return Builder.VERSION;
    }

    @Override
    public DataContainer toContainer() {
        DataContainer container = DataContainer.createNew()
                .set(ITEM_TYPE, itemType)
                .set(MODEL_PATH, modelPath);

        if(modelSettings != null)
            container.set(MODEL_SETTINGS, modelSettings);

        return container;
    }

    public static class Builder extends AbstractDataBuilder<Model> {
        public final static int VERSION = 1;

        public Builder() {
            super(Model.class, VERSION);
        }

        @Override
        protected Optional<Model> buildContent(final DataView container) throws InvalidDataException {
            Trumos.instance.getPlugin().getLogger().info("Model:Builder - " + container.contains(ITEM_TYPE) + ", " + container.contains(MODEL_PATH));
            if(!container.contains(ITEM_TYPE, MODEL_PATH))
                return Optional.empty();

            final ItemType itemType = container.getRegistryValue(ITEM_TYPE, RegistryTypes.ITEM_TYPE).get();
            final String modelPath = container.getString(MODEL_PATH).get();
            final String modelSettings = container.getString(MODEL_SETTINGS).orElse(null);

            return Optional.of(new ModelImpl(itemType, modelPath, modelSettings));
        }
    }
}
