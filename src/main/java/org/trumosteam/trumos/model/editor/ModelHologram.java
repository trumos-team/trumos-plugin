package org.trumosteam.trumos.model.editor;

import org.spongepowered.math.vector.Vector3d;
import org.trumosteam.trumos.api.model.Model;

public class ModelHologram {
    public final Model model;

    public Vector3d position;
    public Vector3d rotation;

    public ModelHologram(final Model model) {
        this.model = model;
        this.position = Vector3d.ZERO;
        this.rotation = Vector3d.ZERO;
    }
}
