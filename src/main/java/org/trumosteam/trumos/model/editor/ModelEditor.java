package org.trumosteam.trumos.model.editor;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.util.Ticks;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Keys;
import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.event.EventContextKeys;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.filter.type.Exclude;
import org.spongepowered.api.event.item.inventory.ChangeInventoryEvent;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.event.network.ServerSideConnectionEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.Hotbar;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.world.SerializationBehavior;
import org.spongepowered.api.world.difficulty.Difficulties;
import org.spongepowered.api.world.server.ServerLocation;
import org.spongepowered.api.world.server.ServerWorld;
import org.spongepowered.api.world.server.WorldTemplate;
import org.spongepowered.math.vector.Vector3d;
import org.spongepowered.math.vector.Vector3i;
import org.trumosteam.trumos.Trumos;
import org.trumosteam.trumos.api.model.Model;
import org.trumosteam.trumos.api.model.ModelInteractionType;
import org.trumosteam.trumos.api.model.ModelSettings;
import org.trumosteam.trumos.util.Packets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class ModelEditor {
    private final Map<UUID, UserData> editors = new HashMap<>();

    private ServerWorld world;
    private ServerLocation centerLocation;
    private Vector3i centerPosition;
    private Entity preview;

    private final ItemStack OPTION_OFFSET_HORIZONTAL = ItemStack.builder().itemType(ItemTypes.STICK).add(Keys.DISPLAY_NAME, Component.text("Move offset horizontal (RMB / RMB + Shift)", NamedTextColor.DARK_PURPLE)).build();
    private final ItemStack OPTION_OFFSET_VERTICAL = ItemStack.builder().itemType(ItemTypes.STICK).add(Keys.DISPLAY_NAME, Component.text("Move offset vertical (RMB / RMB + Shift)", NamedTextColor.DARK_PURPLE)).build();
    private final ItemStack OPTION_COLLISION = ItemStack.builder().itemType(ItemTypes.BLAZE_ROD).add(Keys.DISPLAY_NAME, Component.text("Add(RMB) / Remove(RMB+Shift) collision point", NamedTextColor.GOLD)).build();
    private final ItemStack OPTION_INTERACTION = ItemStack.builder().itemType(ItemTypes.PAPER).add(Keys.DISPLAY_NAME, Component.text("Change interaction type", NamedTextColor.AQUA)).build();
    private final ItemStack OPTION_CLOSE = ItemStack.builder().itemType(ItemTypes.BARRIER).add(Keys.DISPLAY_NAME, Component.text("Close editor", NamedTextColor.RED)).build();

    public final BlockState EDITOR_BLOCK = BlockState.builder().blockType(BlockTypes.GLASS).build();
    public final BlockState COLLISION_BLOCK = BlockState.builder().blockType(BlockTypes.BARRIER).build();

    public ModelEditor(final String worldName) throws IOException {
        final ResourceKey worldKey = ResourceKey.of("models", worldName);

        final WorldTemplate worldTemplate = WorldTemplate.builder()
                .key(worldKey)
                .gameMode(GameModes.CREATIVE)
                .difficulty(Difficulties.PEACEFUL)
//                .generatorModifierType(GeneratorModifierTypes.NONE)
                .serializationBehavior(SerializationBehavior.NONE)
                .build();

        Sponge.getServer().getWorldManager().loadWorld(worldTemplate).whenComplete(((serverWorld, throwable1) -> {
            world = serverWorld;
            centerLocation = world.getLocation(world.getProperties().spawnPosition().toDouble().add(new Vector3d(0.5d, 0d, 0.5d)));
            centerPosition = centerLocation.getBlockPosition();

            world.getBorder().setCenter(centerLocation.getX(), centerLocation.getZ());
            world.getBorder().setDiameter(9);

            final BlockState floor = BlockState.builder().blockType(BlockTypes.DIRT).build();

            for(int i = 0; i < 9; ++i) {
                for(int j = 0; j < 9; ++j)
                    world.setBlock(centerPosition.getX() + i - 4, centerPosition.getY()-1, centerPosition.getZ() + j - 4, floor);
            }

            preview = centerLocation.createEntity(EntityTypes.ARMOR_STAND.get());
            preview.offer(Keys.IS_SMALL, true);
            preview.offer(Keys.INVULNERABLE, true);
            preview.offer(Keys.IS_INVISIBLE, true);
            preview.offer(Keys.IS_GRAVITY_AFFECTED, false);
            world.spawnEntity(preview);
        }));
    }

    public void open(final ServerPlayer player, final Model model) {
        Optional<ModelSettings> modelSettings = model.getSettings();

        if(!modelSettings.isPresent()) {
            player.sendMessage(Component.text("Target model use unregistered ModelSettings!", NamedTextColor.RED));
            return;
        }

        editors.put(player.getUniqueId(), new UserData(player, modelSettings.get()));

        player.getInventory().clear();
        player.setLocation(centerLocation.sub(new Vector3d(0d, 0d, 2d)));
        player.setRotation(new Vector3d(0d, 0d, 0d));
        player.offer(Keys.VANISH, true);

        Hotbar hotbar = player.getInventory().getHotbar();
        hotbar.set(0, OPTION_OFFSET_HORIZONTAL);
        hotbar.set(1, OPTION_OFFSET_VERTICAL);
        hotbar.set(2, OPTION_COLLISION);
        hotbar.set(3, OPTION_INTERACTION);
        hotbar.set(8, OPTION_CLOSE);

        Task.builder()
                .execute(() -> {
                    Packets.setEntityHelmet(player, preview, model.toItemStack());
                    Packets.moveEntity(player, preview, centerLocation.add(modelSettings.get().getOffset()));

                    for(Vector3i point : modelSettings.get().getCollisionPoints())
                        player.sendBlockChange(centerPosition.add(point), EDITOR_BLOCK);
                })
                .delay(Ticks.duration(Ticks.TICKS_PER_SECOND))
                .plugin(Trumos.instance.getPlugin())
                .build();
    }

    public void close(Player player) {
        UserData userData = editors.remove(player.getUniqueId());

        for (Vector3i point : userData.settings.getCollisionPoints())
            player.resetBlockChange(centerPosition.add(point));

        player.getInventory().clear();
        player.setLocation(userData.previousLocation);
        player.offer(Keys.VANISH, false);

        userData.copyInventoryTo(player.getInventory());

        try {
            userData.settings.save();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isEditing(Player player) {
        return editors.containsKey(player.getUniqueId());
    }

    private static class UserData {
        Map<Integer, ItemStack> inventory = new HashMap<>();
        Iterator<ModelInteractionType> interaction;

        ServerLocation previousLocation;
        ModelSettings settings;

        UserData(Player player, ModelSettings settings) {
            this.interaction = Model.INTERACTION_TYPE_REGISTRY.get().stream().iterator();

            this.previousLocation = (ServerLocation) player.getLocation();
            this.settings = settings;

            int i = 0;
            for (Inventory slot : player.getInventory().slots()) {
                ItemStack itemStack = slot.peek();

                if(!itemStack.isEmpty())
                    inventory.put(i, itemStack);

                i++;
            }
        }

        void copyInventoryTo(Inventory container) {

            int i = 0;
            for (Inventory slot : container.slots()) {
                if(inventory.containsKey(i))
                    slot.set(i, inventory.get(i));

                i++;
            }
        }
    }

    @Listener
    @Exclude(ChangeInventoryEvent.Held.class)
    public void onInventoryChange(ChangeInventoryEvent event, @Root Player player) {
        if(editors.containsKey(player.getUniqueId()))
            event.setCancelled(true);
    }

    @Listener
    public void onDestroyBlock(final ChangeBlockEvent.Pre event, @Root Player player) {
        //TODO: Get block transaction
        if(editors.containsKey(player.getUniqueId()))
            event.setCancelled(true);
    }

    @Listener
    public void onPlayerDisconnect(final ServerSideConnectionEvent.Disconnect event) {
        final Player player = event.getPlayer();

        if(editors.containsKey(player.getUniqueId()))
            close(player);
    }

    @Listener
    public void onItemInteract(final InteractItemEvent.Secondary event, final @Root ServerPlayer player) {
        final UUID uuid = player.getUniqueId();
        final HandType usedHand = event.getContext().get(EventContextKeys.USED_HAND).orElse(HandTypes.OFF_HAND.get());

        if(editors.containsKey(uuid)) {
            event.setCancelled(true);

            if(usedHand.equals(HandTypes.MAIN_HAND)) {
                ItemStack itemStack = player.getItemInHand(HandTypes.MAIN_HAND);
                if (itemStack.isEmpty())
                    return;

                /*RayTrace.block().sourceEyePosition(player).
                RayCast.Hit hit = RayCast.from(player, 5);
                if(!hit.collided)
                    return;

                if (itemStack.equalTo(OPTION_OFFSET_HORIZONTAL))
                    updateOffsetHorizontal(player, hit.location.getPosition());
                else if(itemStack.equalTo(OPTION_OFFSET_VERTICAL))
                    updateOffsetVertical(player, hit.location.getPosition());
                else if (itemStack.equalTo(OPTION_COLLISION))
                    updateCollision(player, hit.location.getPosition());
                else if(itemStack.equalTo(OPTION_INTERACTION))
                    changeInteraction(player);
                else if (itemStack.equalTo(OPTION_CLOSE))
                    close(player);*/
            }
        }
    }

    private void updateOffsetHorizontal(Player player, Vector3d interactPoint) {
        UserData data = editors.get(player.getUniqueId());

        if(!player.get(Keys.IS_SNEAKING).orElse(false)) {
            Vector3d relativePosition = centerPosition.toDouble().add(data.settings.getOffset());
            interactPoint = relativePosition.add(getDirectionBetween(interactPoint.floor(), relativePosition));
        }
        else
            interactPoint = interactPoint.floor();

        data.settings.setOffset(interactPoint.sub(centerPosition.toDouble()));
        Packets.teleportEntity(player, preview, centerLocation.add(data.settings.getOffset()), preview.getRotation());
    }

    private void updateOffsetVertical(Player player, Vector3d interactPoint) {
        UserData data = editors.get(player.getUniqueId());

        if(!player.get(Keys.IS_SNEAKING).orElse(false))
            data.settings.setOffset(data.settings.getOffset().add(new Vector3d(0d, 0.02d, 0d)));
        else
            data.settings.setOffset(data.settings.getOffset().add(new Vector3d(0d, -0.02d, 0d)));

        Packets.teleportEntity(player, preview, centerLocation.add(data.settings.getOffset()), preview.getRotation());
    }

    private void updateCollision(Player player, Vector3d interactPoint) {
        UserData data = editors.get(player.getUniqueId());
        ServerLocation targetLocation = world.getLocation(interactPoint);

        if(targetLocation.getBlockType().equals(BlockTypes.GRASS.get()))
            return;

        Vector3i blockPosition = targetLocation.getBlockPosition();
        Vector3i localPosition = blockPosition.sub(centerLocation.getBlockPosition());

        boolean isSneaking = player.getOrElse(Keys.IS_SNEAKING, false);
        boolean pointExists = data.settings.getCollisionPoints().contains(localPosition);

        if(!isSneaking && !pointExists) {
            data.settings.getCollisionPoints().add(localPosition);
            player.sendBlockChange(blockPosition, EDITOR_BLOCK);

        }
        else if(isSneaking && pointExists) {
            data.settings.getCollisionPoints().remove(localPosition);
            player.resetBlockChange(blockPosition);
        }
    }

    private void changeInteraction(final Player player) {
        UserData data = editors.get(player.getUniqueId());

        if(!data.interaction.hasNext())
            data.interaction = Model.INTERACTION_TYPE_REGISTRY.get().stream().iterator();

        ModelInteractionType interactionType = data.interaction.next();
        data.settings.setInteractionType(interactionType);

        player.sendActionBar(Component.text("Interaction type: " + Model.INTERACTION_TYPE_REGISTRY.get().valueKey(interactionType).asString(), NamedTextColor.AQUA));
    }

    private Vector3d getDirectionBetween(final Vector3d first, final Vector3d second) {
        double x = first.getX() - second.getX(), y = first.getY() - second.getY(), z = first.getZ() - second.getZ();

        if(x != 0) x = (x/-Math.abs(x)) * -0.1d;
        if(y != 0) y = (y/-Math.abs(y)) * -0.1d;
        if(z != 0) z = (z/-Math.abs(z)) * -0.1d;

        return new Vector3d(x, y, z);
    }
}
