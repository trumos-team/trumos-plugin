package org.trumosteam.trumos.util;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class ZipFile {
    private static final byte[] buffer = new byte[1024];

    private final OutputStream stream;
    private final ZipOutputStream zipStream;

    public ZipFile(String output) throws IOException {
        stream = new FileOutputStream(new File(output));
        zipStream = new ZipOutputStream(stream);
    }

    public void addDirectory(String path) {
        try { zipStream.putNextEntry(new ZipEntry(path.endsWith(File.separator) ? path : path + File.separator)); }
        catch (Exception ignored) {}
    }

    public void addFile(String path, String buffer) throws IOException {
        zipStream.putNextEntry(new ZipEntry(path));
        zipStream.write(buffer.getBytes(), 0, buffer.length());
        zipStream.closeEntry();
    }

    public void addFile(String path, File file) throws IOException {
        zipStream.putNextEntry(new ZipEntry(path));

        FileInputStream fileStream = new FileInputStream(file);
        int length;

        while ((length = fileStream.read(buffer)) > 0) {
            zipStream.write(buffer, 0, length);
        }

        zipStream.closeEntry();
    }

    public void close() throws IOException {
        zipStream.close();
        stream.close();
    }
}

