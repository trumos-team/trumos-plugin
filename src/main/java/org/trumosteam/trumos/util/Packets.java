package org.trumosteam.trumos.util;

import com.mojang.datafixers.util.Pair;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.network.play.server.*;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.server.ServerLocation;
import org.spongepowered.common.item.util.ItemStackUtil;
import org.spongepowered.math.vector.Vector3d;
import org.trumosteam.trumos.mixin.SEntityTeleportPacketAccessor;
import org.trumosteam.trumos.mixin.SSpawnMobPacketAccessor;

import java.util.Collections;

public class Packets {
    public static void spawnEntity(final Player player, final Entity entity, final ServerLocation location, final Vector3d rotation) {
        final LivingEntity living = (LivingEntity) entity;
        final Vector3d position = location.getPosition();

        final SSpawnMobPacket spawnPacket = new SSpawnMobPacket(living);
        ((SSpawnMobPacketAccessor)spawnPacket).accessor$setX(position.getX());
        ((SSpawnMobPacketAccessor)spawnPacket).accessor$setY(position.getY());
        ((SSpawnMobPacketAccessor)spawnPacket).accessor$setZ(position.getZ());
        ((SSpawnMobPacketAccessor)spawnPacket).accessor$setYRot((byte)((int)(rotation.getY() * 256.0F / 360.0F)));
        ((SSpawnMobPacketAccessor)spawnPacket).accessor$setXRot((byte)((int)(rotation.getX() * 256.0F / 360.0F)));

        final SEntityMetadataPacket metadataPacket = new SEntityMetadataPacket(living.getId(), living.getEntityData(), true);

        ((ServerPlayerEntity) player).connection.send(spawnPacket);
        ((ServerPlayerEntity) player).connection.send(metadataPacket);
    }

    public static void unSpawnEntity(final Player player, final Entity entity) {
        final LivingEntity living = (LivingEntity) entity;
        final SDestroyEntitiesPacket packet = new SDestroyEntitiesPacket(living.getId());

        ((ServerPlayerEntity) player).connection.send(packet);
    }

    public static void teleportEntity(final Player player, final Entity entity, final ServerLocation location, final Vector3d rotation) {
        final LivingEntity living = (LivingEntity) entity;
        final Vector3d position = location.getPosition();

        final SEntityTeleportPacket packet = new SEntityTeleportPacket(living);
        ((SEntityTeleportPacketAccessor)packet).accessor$setX(position.getX());
        ((SEntityTeleportPacketAccessor)packet).accessor$setY(position.getY());
        ((SEntityTeleportPacketAccessor)packet).accessor$setZ(position.getZ());
        ((SEntityTeleportPacketAccessor)packet).accessor$setYRot((byte)((int)(rotation.getY() * 256.0F / 360.0F)));
        ((SEntityTeleportPacketAccessor)packet).accessor$setXRot((byte)((int)(rotation.getX() * 256.0F / 360.0F)));

        ((ServerPlayerEntity) player).connection.send(packet);
    }

    public static void moveEntity(final Player player, final Entity entity, final ServerLocation location) {
        final LivingEntity living = (LivingEntity) entity;
        final Vector3d position = location.getPosition();

        final SEntityTeleportPacket packet = new SEntityTeleportPacket(living);
        ((SEntityTeleportPacketAccessor)packet).accessor$setX(position.getX());
        ((SEntityTeleportPacketAccessor)packet).accessor$setY(position.getY());
        ((SEntityTeleportPacketAccessor)packet).accessor$setZ(position.getZ());

        ((ServerPlayerEntity) player).connection.send(packet);
    }

    public static void setEntityHelmet(final Player player, final Entity entity, final ItemStack itemStack) {
        final LivingEntity living = (LivingEntity) entity;
        final SEntityEquipmentPacket packet = new SEntityEquipmentPacket(living.getId(), Collections.singletonList(new Pair<>(EquipmentSlotType.HEAD, ItemStackUtil.toNative(itemStack))));

        ((ServerPlayerEntity) player).connection.send(packet);
    }
}
