package org.trumosteam.trumos;

import io.leangen.geantyref.TypeToken;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.Command;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.parameter.Parameter;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.server.ServerPlayer;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.registry.RegistryTypes;
import org.trumosteam.trumos.api.model.Model;
import org.trumosteam.trumos.api.model.ModelService;
import org.trumosteam.trumos.api.model.ModelSettings;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public class Commands {
    private static final Parameter.Value<ItemType> ITEM_TYPE_PARAMETER = Parameter.registryElement(TypeToken.get(ItemType.class), RegistryTypes.ITEM_TYPE).setKey("item type").build();
    private static final Parameter.Value<ModelSettings> MODEL_SETTINGS_PARAMETER = Parameter.choices(ModelSettings.class, Trumos.instance.getModelService().getAllSettings()).setKey("model settings").optional().build();
    private static final Parameter.Value<String> MODEL_PATH_PARAMETER = Parameter.string().setKey("model path").build();
    private static final Parameter.Value<String> NAME_PARAMETER = Parameter.string().setKey("name").build();

    private static Command.Parameterized registerCommand() {
        return Command.builder()
                .setPermission("models.register")
                .parameters(ITEM_TYPE_PARAMETER, MODEL_PATH_PARAMETER)
                .setExecutor(context -> {
                    final ItemType itemType = context.requireOne(ITEM_TYPE_PARAMETER);
                    final String modelPath = context.requireOne(MODEL_PATH_PARAMETER);

                    try {
                        Trumos.instance.getModelService().registerModel(itemType, modelPath);
                        context.sendMessage(Identity.nil(), Component.text("Successfully registered model!", NamedTextColor.GREEN));

                    } catch (IllegalArgumentException e) {
                        context.sendMessage(Identity.nil(), Component.text(e.getMessage(), NamedTextColor.RED));
                    }

                    return CommandResult.success();
                })
                .build();
    }

    private static Command.Parameterized unregisterCommand() {
        return Command.builder()
                .setPermission("models.unregister")
                .parameters(ITEM_TYPE_PARAMETER, MODEL_PATH_PARAMETER)
                .setExecutor(context -> {
                    final ItemType itemType = context.requireOne(ITEM_TYPE_PARAMETER);
                    final String modelPath = context.requireOne(MODEL_PATH_PARAMETER);

                    if(Trumos.instance.getModelService().unregisterModel(itemType, modelPath))
                        context.sendMessage(Identity.nil(), Component.text("Successfully unregistered model!", NamedTextColor.GREEN));
                    else
                        context.sendMessage(Identity.nil(), Component.text("Specified model wasn't registered!", NamedTextColor.GOLD));

                    return CommandResult.success();
                })
                .build();
    }

    private static Command.Parameterized packCommand() {
        return Command.builder()
                .setPermission("models.pack")
                .setExecutor(context -> {
                    Sponge.getServer().getBroadcastAudience().sendMessage(Component.text("Packing Resource-Pack...", NamedTextColor.GOLD));

                    try {
                        Trumos.instance.getModelService().buildResourcePack();
                    }
                    catch (IOException e) {
                        Sponge.getServer().getBroadcastAudience().sendMessage(Component.text("An error occured while preparing Resource-Pack!", NamedTextColor.RED));
                        e.printStackTrace();

                        return CommandResult.success();
                    }

                    Sponge.getServer().getBroadcastAudience().sendMessage(Component.text("Resource-Pack packed successfully!", NamedTextColor.GREEN));
                    return CommandResult.success();
                })
                .build();
    }

    private static Command.Parameterized getCommand() {
        return Command.builder()
                .setPermission("models.get")
                .parameters(ITEM_TYPE_PARAMETER, MODEL_PATH_PARAMETER, MODEL_SETTINGS_PARAMETER)
                .setExecutor(context -> {
                    if(context.getCause().root() instanceof Player) {
                        final Player player = (Player) context.getCause().root();

                        final ItemType itemType = context.requireOne(ITEM_TYPE_PARAMETER);
                        final String modelPath = context.requireOne(MODEL_PATH_PARAMETER);

                        final Optional<ModelSettings> modelSettings = context.getOne(MODEL_SETTINGS_PARAMETER);

                        final Model model = Model.builder()
                                .itemType(itemType)
                                .modelPath(modelPath)
                                .build();

                        modelSettings.ifPresent(model::setSettings);
                        player.getInventory().offer(model.toItemStack());
                    }

                    return CommandResult.success();
                })
                .build();
    }

    private static Command.Parameterized uploadCommand() {
        return Command.builder()
                .setPermission("models.upload")
                .setExecutor(context -> {
                    Sponge.getServer().getBroadcastAudience().sendMessage(Identity.nil(), Component.text("Uploading Resource-Pack...", NamedTextColor.GOLD));

                    try {
                        Trumos.instance.getModelService().uploadResourcePack();
                    }
                    catch (IOException e) {
                        Sponge.getServer().getBroadcastAudience().sendMessage(Identity.nil(), Component.text("An error occured while trying to upload Resource-Pack!", NamedTextColor.RED));
                        e.printStackTrace();

                        return CommandResult.success();
                    }

                    Sponge.getServer().getBroadcastAudience().sendMessage(Identity.nil(), Component.text("Resource-Pack uploaded successfully!", NamedTextColor.GREEN));
                    return CommandResult.success();
                })
                .build();
    }

    private static Command.Parameterized addSettingsCommand() {
        return Command.builder()
                .parameter(NAME_PARAMETER)
                .setExecutor(context -> {
                    final ModelService modelService = Trumos.instance.getModelService();
                    final String name = context.requireOne(NAME_PARAMETER);

                    if(!modelService.getSettings(name).isPresent()) {
                        try {
                            modelService.registerSettings(ModelSettings.builder().build(name));
                        }
                        catch (IOException | SQLException e) {
                            context.sendMessage(Identity.nil(), Component.text("Error occured while trying to register model settings! (More info in console)", NamedTextColor.RED));
                            Trumos.instance.getPlugin().getLogger().error("Error occurred while trying to register model settings: ", e);
                        }

                        context.sendMessage(Identity.nil(), Component.text("Successfully created and registered model settings with name '" + name + "'!", NamedTextColor.GREEN));
                    }
                    else
                        context.sendMessage(Identity.nil(), Component.text("Model Settings with specified name is already registered!", NamedTextColor.RED));

                    return CommandResult.success();
                })
                .build();
    }

    private static Command.Parameterized removeSettingsCommand() {
        return Command.builder()
                .parameter(NAME_PARAMETER)
                .setExecutor(context -> {
                    final String name = context.requireOne(NAME_PARAMETER);

                    final ModelService modelService = Trumos.instance.getModelService();
                    final Optional<ModelSettings> modelSettings = modelService.getSettings(name);

                    if(modelSettings.isPresent()) {
                        modelService.unregisterSettings(modelSettings.get());
                        context.sendMessage(Identity.nil(), Component.text("Successfully removed model settings with name '" + name + "'!", NamedTextColor.GREEN));
                    }
                    else
                        context.sendMessage(Identity.nil(), Component.text("Model Settings with specified name doesn't exists!", NamedTextColor.RED));


                    return CommandResult.success();
                })
                .build();
    }

    private static Command.Parameterized editorCommand() {
        return Command.builder()
                .setPermission("models.editor")
                .setExecutor(context -> {
                    if(context.getCause().root() instanceof Player) {
                        final ServerPlayer player = (ServerPlayer) context.getCause().root();
                        final Optional<Model> model = player.getItemInHand(HandTypes.MAIN_HAND).get(Model.DATA);

                        if(Trumos.instance.getModelEditor().isEditing(player))
                            Trumos.instance.getModelEditor().close(player);
                        else if(model.isPresent()) {
                            Trumos.instance.getModelEditor().open(player, model.get());
                        }
                        else
                            player.sendMessage(Identity.nil(), Component.text("In order to edit model, you need to hold it in your main-hand!", NamedTextColor.RED));
                    }

                    return CommandResult.success();
                })
                .child(addSettingsCommand(), "add")
                .child(removeSettingsCommand(), "remove")
                .build();
    }

    static Command.Parameterized rootCommand() {
        return Command.builder()
                .setPermission("models")
                .child(registerCommand(), "register")
                .child(unregisterCommand(), "unregister")
                .child(getCommand(), "get")
                .child(packCommand(), "pack")
                .child(uploadCommand(), "upload")
                .child(editorCommand(), "editor")
                .build();
    }
}
